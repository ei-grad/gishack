#!/bin/bash

mkdir train_image train_mask train_target

mv TRAINING/*/*_mask.png train_mask
for i in train_mask/0/*; do mv $i ${i%%_mask.png}.png; done

mv TRAINING/*/*.png train_image
mv TRAINING/*/*.csv train_target
